Apeiron LaTeX Class and Style
=============================

Description
-----------

Simple, non-technical, and visually appealing LaTeX theme, with a custom title
and some touches of color.

In order to improve compatibility, it has been tried to use a small amount of
LaTeX packages (those provided by the `texlive-latex-base` package in
Debian/Ubuntu/Mint linux distributions). Instead, it has been redefined the
proper LaTeX commands from article class. For this reason, it's possible to
obtain some unexpected results in certain singular uses of the modified
commands.

**Required packages**: `graphicx`, `fancyhdr`, `geometry`, `hyperref`, `xcolor`,
`kvoptions` and `ifthen`.

**Redefined commands**: `\maketitle`, `\section`, `\subsection`,
`\@seccntformat`, `\headrulewidth`, `\author`, `\@makecaption` and
`\tableofcontents`.

**Custom commands/environments**:

-   `HLPar`: Environment to highlight an important paragraph of text. It uses de
    environment `minipage` and some LaTeX boxes; as a consequence, there are
    some restrictions: you can not use floating environments (like `figure` or
    `table`) within it. Usage:

        \HLPar{Paragraph-to-Highlight}[Optional-Title]

-   `\contact`: Command to get contact info (e-mail or web page ...) and display
    the info in the title.

-   `\HideSecNumbers`: Command to hide the section numbers.

-   `\TitleSizeFactor`: Command to adjust the proportions of the title
    decorations.

**Other Features**: 

-   By default, line spacing is automatically adjusted to the font size of the
    document.

-   The proportions of the title decorations are automatically adjusted to the
    font size of the document


Supported Class Options
-----------------------

Since version 1.1 the **kvoptions** package is used to manage class options,
allowing to input parameters with a `<key>=<value>` syntax.

**Article Class Options**: All article class options are accepted, except
`titlepage` and `twocolumn`.

**Custom Options**:

-   `TColor`: key-val option used to highlight text in headers, footers, links,
    and section titles. Supported values: `Blue`, `Maroon` and `Black`.

-   `CCLic`: key-val option used to insert the icons for the corresponding
    Creative Commons License in the title of the document. The supported values
    are `CCBY`, `CCBYSA`, `CCBYND`, `CCBYNC`, `CCBYNCSA` and `CCBYNCND`.

-   `secnumbers`, `nosecnumbers`: complementary option to show/hide the section
    numbers.

Example:

    \documentclass[a4paper,11pt,TColor=Maroon,CCLic=CCBYSA]{apeiron}

Screenshots
-----------
Some captures of the theme:

![apeiron-1](apeiron-1.png)

![apeiron-2](apeiron-2.png)



License
-------

This project is licensed under the MIT license.
