%% Apeiron laTeX Class 
%% v0.9, 2018/08/22
%% v1.0, 2018/08/29
%% v1.1, 2019/01/27
%% Víctor Huertas, http://reduccionalabsurdo.es
%%
%% Required packages: arche, kvoptions and ifthen.
%% Supported options:
%%  - From LaTex standard article: 10pt, 11pt, 12pt, a4paper, letterpaper.
%%  - Custom options: 
%%    -- Color schemes. "TColor = <color>", where <color> should take one of
%%       the three values: Blue, Maroon or Black.
%%    -- CC License images. "CCLic = <lic>", where <lic> should take one of
%%       the values: None, CCBY, CCBYSA, CCBYND, CCBYNC, CCBYNCSA or CCBYNCND. 
%%    -- Section Numbers. Use "secnumbers" or "nosecnumbers" to show/hide
%%       the section numbers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{apeiron}[2019/01/27 v1.1 Apeiron]
\typeout{Apeiron Document Class}
\typeout{v1.1, 2019/01/27}
\typeout{Víctor Huertas}


%% Load required packages.

\RequirePackage{kvoptions}
\RequirePackage{ifthen}


% Kvoptions configuration

\SetupKeyvalOptions{
  family=aprn,
  prefix=aprn@
}

%% LaTeX options from article class

\newcommand{\TSF}{100}

\DeclareVoidOption{10pt}{
  \PassOptionsToClass{\CurrentOption}{article}
}

\DeclareVoidOption{11pt}{
  \PassOptionsToClass{\CurrentOption}{article}
  \AtEndOfClass{
    \linespread{1.3}
  }
  \renewcommand{\TSF}{115}
}

\DeclareVoidOption{12pt}{
  \PassOptionsToClass{\CurrentOption}{article}
  \AtEndOfClass{
    \linespread{1.6}
  }
  \renewcommand{\TSF}{130}
}

\DeclareVoidOption{a4paper}{
  \PassOptionsToClass{\CurrentOption}{article}
}

\DeclareVoidOption{letterpaper}{
  \PassOptionsToClass{\CurrentOption}{article}
}


\DeclareVoidOption{twocolumn}{
  \ClassError{apeiron}{Unknown option '\CurrentOption'}
  {Option not allowed for this class}
}

\DeclareVoidOption{titlepage}{
  \ClassError{apeiron}{Unknown option '\CurrentOption'}
  {Option not allowed for this class}
}



%% CUSTOM OPTIONS

%% Creative Commons Licences (String Option provided by kvoptions)

\DeclareStringOption[None]{CCLic}[None]


%% Template color (String Option  provided by kvoptions)

\DeclareStringOption[Blue]{TColor}[Blue]


%% Complementary option to show/hide section numbers

\DeclareBoolOption[true]{nosecnumbers}
\DeclareComplementaryOption{secnumbers}{nosecnumbers}


%% Fallback Options

\DeclareDefaultOption{
  \PassOptionsToClass{\CurrentOption}{article}
  \ClassWarning{apeiron}{Unknown option '\CurrentOption'}
  }


%% Processing given options
%% \ProcessOptions\relax
\ProcessKeyvalOptions{aprn}\relax


%% Check if a supported value has been inserted for the 'TColor' option in
%% the '\documentclass' command.

\ifthenelse{\NOT\equal{\aprn@TColor}{Blue} \AND \NOT\equal{\aprn@TColor}{Maroon}%
\AND \NOT\equal{\aprn@TColor}{Black}}
{%
  \ClassWarning{apeiron}{Unknown option 'TColor=\aprn@TColor'; using the default value 'TColor=Blue'.}%
  \renewcommand\aprn@TColor{Blue}%
}\relax


%% Check if a supported value has been inserted for the 'CCLic' option in
%% the '\documentclass' command.

\ifthenelse{\NOT\equal{\aprn@CCLic}{None} \AND %
\NOT\equal{\aprn@CCLic}{CCBY} \AND \NOT\equal{\aprn@CCLic}{CCBYSA}%
\AND \NOT\equal{\aprn@CCLic}{CCBYND} \AND \NOT\equal{\aprn@CCLic}{CCBYNC}
\AND \NOT\equal{\aprn@CCLic}{CCBYNCSA} \AND \NOT\equal{\aprn@CCLic}{CCBYNCND}}
{\ClassWarning{apeiron}{Unknown option '\aprn@CCLic'; using the default value%
'CCLic=None'.}}\relax


%% Load base class

\LoadClass[a4paper]{article}


%% Load package arche

\RequirePackage{arche}


%% Set the theme color 

\UseThemeColor{\aprn@TColor}


%% Set the CC license of the document

\UseCCLicense{\aprn@CCLic}


%% Hide the section numbers when required

\ifaprn@nosecnumbers
  \HideSecNumbers
\fi


%% Set the proportions of the title decorations according to the font size of the document

\SetTitleSizeFactor{\TSF}

\endinput
